fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .compile_well_known_types(true)
        .compile(
            &[
                "../base_proto/google.proto",
                "../base_proto/base/base.proto",
                "../base_proto/robot_core/robot_core.proto",
                "../base_proto/node/node.proto",
            ],
            &["../base_proto/"],
        )?;
    Ok(())
}
