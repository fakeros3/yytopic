use robot_core::robot_core_client::RobotCoreClient;
use structopt::clap::AppSettings;
use structopt::StructOpt;
use tonic::transport::Channel;

pub mod base {
    tonic::include_proto!("base");
}

pub mod robot_core {
    tonic::include_proto!("robot_core");
}

pub mod google {
    pub mod protobuf {
        tonic::include_proto!("google.protobuf");
    }
}

/// yytopic is a command-line tool for printing information about Topics
#[derive(StructOpt, Debug)]
#[structopt(setting = AppSettings::ColoredHelp)]
#[structopt(setting = AppSettings::DisableVersion)]
#[structopt(verbatim_doc_comment)]
enum Opt {
    /// list active topics
    #[structopt(setting = AppSettings::ColoredHelp)]
    #[structopt(setting = AppSettings::DisableVersion)]
    #[structopt(setting = AppSettings::ArgRequiredElseHelp)]
    List {
        /// list all topics
        #[structopt(short, long)]
        all: bool,
        /// the detail of topic
        #[structopt(required_if("all", "false"))]
        topic: Option<String>,
    },

    /// display publishing rate of topic
    #[structopt(setting = AppSettings::ColoredHelp)]
    #[structopt(setting = AppSettings::DisableVersion)]
    #[structopt(setting = AppSettings::ArgRequiredElseHelp)]
    Hz {
        /// the topic
        topic: String,
    },

    /// print messages of screen
    #[structopt(setting = AppSettings::ColoredHelp)]
    #[structopt(setting = AppSettings::DisableVersion)]
    #[structopt(setting = AppSettings::ArgRequiredElseHelp)]
    Echo {
        /// show history topics
        #[structopt(short, long)]
        latch: bool,
        /// the row size of array
        #[structopt(short, long)]
        row: Option<u32>,
        /// the topic
        topic: String,
    },

    /// publish data to topic
    #[structopt(setting = AppSettings::ColoredHelp)]
    #[structopt(setting = AppSettings::DisableVersion)]
    #[structopt(setting = AppSettings::ArgRequiredElseHelp)]
    Pub {
        /// the topic
        topic: String,
        /// the msg (format json)
        #[structopt(default_value = "{}")]
        msg: String,
    },
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let core_address = std::env::var("YY_CORE_ENDPOINT").unwrap_or("[::1]:11311".to_string());
    let core_address = format!("http://{}", core_address);
    println!("connecting to robot core: {}", core_address);

    if let Ok(mut cli) = RobotCoreClient::connect(core_address.clone()).await {
        match opt {
            Opt::Echo { latch, row, topic } => core_echo(&mut cli, topic, latch, row).await?,
            Opt::Pub { topic, msg } => core_pub(&mut cli, topic, msg).await?,
            Opt::Hz { topic } => core_hz(&mut cli, topic).await?,
            Opt::List { all, topic } => core_list(&mut cli, all, topic).await?,
        }
    } else {
        println!("Connect to robot_core at {} failed !!!", core_address);
        println!("You should check robot_core if it has started.");
        println!(
            "Or if robot_core has already started at remote host, you can set YY_CORE_ENDPOINT:"
        );
        println!("\t`export YY_CORE_ENDPOINT=<ip:port>'");
    }

    Ok(())
}

async fn core_echo(
    cli: &mut RobotCoreClient<Channel>,
    topic: String,
    latch: bool,
    row: Option<u32>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut req = tonic::Request::new(robot_core::SubscribeRequest {
        topic,
        latch,
        count: 1,
    });
    req.metadata_mut()
        .insert("__istools", "true".parse().unwrap());
    req.metadata_mut()
        .insert("__node", "yytopic".parse().unwrap());
    // req.set_timeout(std::time::Duration::from_secs(3));

    let mut stream = cli.subscribe(req).await?.into_inner();

    while let Some(msg) = stream.message().await? {
        let msg = msg.msg;
        if !msg.is_empty() {
            println!("-----------------------------------------------------------------");
            let v: serde_json::Value = serde_json::from_str(&msg)?;
            let r = if let Some(r) = row { r } else { 12 };
            println!("{}", format_json(0, &v, r as usize));
        }
    }

    Ok(())
}

async fn core_list(
    cli: &mut RobotCoreClient<Channel>,
    all: bool,
    topic: Option<String>,
) -> Result<(), Box<dyn std::error::Error>> {
    let topic = if all { Option::<String>::None } else { topic };
    let mut req = tonic::Request::new(robot_core::ListTopicRequest { topic });
    req.set_timeout(std::time::Duration::from_secs(3));

    let resp = cli.list_topic(req).await?.into_inner();

    if resp.topics.len() == 0 {
        println!("no topics");
    } else if resp.topics.len() == 1 {
        let detail = &resp.topics[0];
        println!("topic - {}:", detail.topic);
        println!("publishers:");
        for p in detail.pubers.iter() {
            println!("  - {}", p);
        }
        println!("subscribers:");
        for s in detail.subers.iter() {
            println!("  - {}", s);
        }
        if !detail.msg.is_empty() {
            println!("sample:");
            println!("  - {}", detail.msg);
        }
    } else {
        println!("all topics:");
        for v in resp.topics.iter() {
            println!("  - {}", v.topic);
        }
    }

    Ok(())
}

async fn core_hz(
    cli: &mut RobotCoreClient<Channel>,
    topic: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut req = tonic::Request::new(robot_core::SubscribeRequest {
        topic,
        latch: false,
        count: 0,
    });
    req.metadata_mut()
        .insert("__istools", "true".parse().unwrap());
    req.metadata_mut()
        .insert("__node", "yytopic".parse().unwrap());

    let mut stream = cli.subscribe(req).await?.into_inner();

    let mut start = std::time::Instant::now();
    let mut count = 0;
    while let Some(msg) = stream.message().await? {
        if msg.msg.is_empty() { continue; }

        count += 1;
        let ms = start.elapsed().as_millis();

        if ms >= 1000 {
            let h: f64 = (count * 1000) as f64 / ms as f64;
            println!("hz: {:.02} in {}ms", h, ms);
            count = 0;
            start = std::time::Instant::now();
        }
    }

    Ok(())
}

async fn core_pub(
    cli: &mut RobotCoreClient<Channel>,
    topic: String,
    msg: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut req = tonic::Request::new(robot_core::PublishRequest { topic, msg });
    req.metadata_mut()
        .insert("__istools", "true".parse().unwrap());
    req.metadata_mut()
        .insert("__node", "yytopic".parse().unwrap());
    req.set_timeout(std::time::Duration::from_secs(3));

    let _resp = cli.publish(req).await?.into_inner();
    Ok(())
}

fn format_json(depth: usize, j: &serde_json::Value, array_size: usize) -> String {
    const INDENT: usize = 2;
    let mut ret: String = "".to_string();

    if j.is_object() {
        let obj = j.as_object().unwrap();
        if obj.is_empty() {
            return if depth == 0 {
                "{}".to_string()
            } else {
                "".to_string()
            };
        }

        for (k, v) in obj.into_iter() {
            ret += &format!("{:ident$}", "", ident = depth * INDENT);
            if v.is_object() || v.is_array() {
                let f = format_json(depth + 1, &v, array_size);
                if f.is_empty() {
                    ret += &format!("{}: null\n", k);
                } else {
                    ret += &format!("{}:\n", k);
                    ret += &format!("{}", f);
                }
            } else {
                ret += &format!("{}: {}\n", k, format_json(depth + 1, &v, array_size));
            }
        }
    } else if j.is_array() {
        let arr = j.as_array().unwrap();
        let sz = arr.len();
        if sz == 0 {
            return "".to_string();
        }
        for i in (0..sz).step_by(array_size) {
            ret += &format!("{:ident$}", "", ident = depth * INDENT);
            let mut sep = "";
            for x in 0..array_size {
                let index = i + x;
                if index >= sz {
                    break;
                }
                ret += &format!("{}{}", sep, arr[index].to_string());
                sep = ", ";
            }
            ret += "\n";
        }
    } else if j.is_null() {
        ret = "null".to_string();
    } else {
        ret = j.to_string();
    }

    ret
}
